import parseUrl from "url-parse"
import win from "core/window"
import { btoa, buildFormData } from "core/utils"

export const SHOW_AUTH_POPUP = "show_popup"
export const AUTHORIZE = "authorize"
export const LOGOUT = "logout"
export const PRE_AUTHORIZE_OAUTH2 = "pre_authorize_oauth2"
export const AUTHORIZE_OAUTH2 = "authorize_oauth2"
export const VALIDATE = "validate"
export const CONFIGURE_AUTH = "configure_auth"
export const RESTORE_AUTHORIZATION = "restore_authorization"
export const SELECT ='selectAccount'

const scopeSeparator = " "

export function showDefinitions(payload) {
  return {
    type: SHOW_AUTH_POPUP,
    payload: payload
  }
}

export function authorize(payload) {
  return {
    type: AUTHORIZE,
    payload: payload
  }
}

export const authorizeWithPersistOption = (payload) => ( { authActions } ) => {
  authActions.authorize(payload)
  authActions.persistAuthorizationIfNeeded()  
}

export function logout(payload) {
  return {
    type: LOGOUT,
    payload: payload
  }
}

export const logoutWithPersistOption = (payload) => ( { authActions } ) => {
  authActions.logout(payload)
  authActions.persistAuthorizationIfNeeded()  
}

export const preAuthorizeImplicit = (payload) => ( { authActions, errActions } ) => {
  let { auth , token, isValid } = payload
  let { schema, name } = auth
  let flow = schema.get("flow")

  // remove oauth2 property from window after redirect from authentication
  delete win.swaggerUIRedirectOauth2

  if ( flow !== "accessCode" && !isValid ) {
    errActions.newAuthErr( {
      authId: name,
      source: "auth",
      level: "warning",
      message: "Authorization may be unsafe, passed state was changed in server Passed state wasn't returned from auth server"
    })
  }

  if ( token.error ) {
    errActions.newAuthErr({
      authId: name,
      source: "auth",
      level: "error",
      message: JSON.stringify(token)
    })
    return
  }

  authActions.authorizeOauth2WithPersistOption({ auth, token })
}


export function authorizeOauth2(payload) {
  return {
    type: AUTHORIZE_OAUTH2,
    payload: payload
  }
}


export const authorizeOauth2WithPersistOption = (payload) => ( { authActions } ) => {
  authActions.authorizeOauth2(payload)
  authActions.persistAuthorizationIfNeeded()  
}

export const authorizePassword = ( auth ) => ( { authActions } ) => {
  let { schema, name, email,accountId,userId,accessToken,refreshToken ,password,accountUserId,applicationId,applicationuserId,secretKey, passwordType, clientId, clientSecret ,selectGrantType } = auth
  let form = {
    grantType: selectGrantType
  }
  let query = {}
  let headers = {}
  if(selectGrantType =='selectAccount'){
    if(accountId !==''){
       Object.assign(form,{accountId})    
    }
    if(userId !==''){
       Object.assign(form,{userId})    
    }
    if(refreshToken !==''){
       Object.assign(form,{refreshToken})    
    }
    if(accessToken !==''){
       Object.assign(form,{accessToken})    
    }
  }else{
    if(email !==''){
      Object.assign(form,{email})    
    }
    if(password !==''){
      Object.assign(form,{password})    
    }
    if(accountUserId !==''){
      Object.assign(form,{accountUserId})    
    }
    if(applicationId !==''){
      Object.assign(form,{applicationId})    
    }
    if(applicationuserId !==''){
      Object.assign(form,{applicationuserId})    
    }
    if(secretKey !==''){
      Object.assign(form,{secretKey})    
    }
  }
  switch (passwordType) {
    case "request-body":
      setClientIdAndSecret(form, clientId, clientSecret)
      break

    case "basic":
      headers.Authorization = "Basic " + btoa(clientId + ":" + clientSecret)
      break
    default:
      console.warn(`Warning: invalid passwordType ${passwordType} was passed, not including client id and secret`)
  }

  return authActions.authorizeRequest({ body: form, url: schema.get("tokenUrl"), name, headers, query, auth})
}

function setClientIdAndSecret(target, clientId, clientSecret) {
  if ( clientId ) {
    Object.assign(target, {client_id: clientId})
  }

  if ( clientSecret ) {
    Object.assign(target, {client_secret: clientSecret})
  }
}

export const authorizeApplication = ( auth ) => ( { authActions } ) => {
  let { schema, scopes, name, clientId, clientSecret } = auth
  let headers = {
    Authorization: "Basic " + btoa(clientId + ":" + clientSecret)
  }
  let form = {
    grant_type: "client_credentials",
    scope: scopes.join(scopeSeparator)
  }

  return authActions.authorizeRequest({body: buildFormData(form), name, url: schema.get("tokenUrl"), auth, headers })
}

export const authorizeAccessCodeWithFormParams = ( { auth, redirectUrl } ) => ( { authActions } ) => {
  let { schema, name, clientId, clientSecret, codeVerifier } = auth
  let form = {
    grant_type: "authorization_code",
    code: auth.code,
    client_id: clientId,
    client_secret: clientSecret,
    redirect_uri: redirectUrl,
    code_verifier: codeVerifier
  }

  return authActions.authorizeRequest({body: buildFormData(form), name, url: schema.get("tokenUrl"), auth})
}

export const authorizeAccessCodeWithBasicAuthentication = ( { auth, redirectUrl } ) => ( { authActions } ) => {
  let { schema, name, clientId, clientSecret } = auth
  let headers = {
    Authorization: "Basic " + btoa(clientId + ":" + clientSecret)
  }
  let form = {
    grant_type: "authorization_code",
    code: auth.code,
    client_id: clientId,
    redirect_uri: redirectUrl
  }

  return authActions.authorizeRequest({body: buildFormData(form), name, url: schema.get("tokenUrl"), auth, headers})
}

export const authorizeRequest = ( data ) => ( { fn, getConfigs, authActions, errActions, oas3Selectors, specSelectors, authSelectors } ) => {
  let { body, query={}, headers={}, name, url, auth } = data
  let getRequestedDataAfterTokenExpiry = body;
  let { additionalQueryStringParams } = authSelectors.getConfigs() || {}

  let parsedUrl

  if (specSelectors.isOAS3()) {
    let finalServerUrl = oas3Selectors.serverEffectiveValue(oas3Selectors.selectedServer())
    parsedUrl = parseUrl(url, finalServerUrl, true)
  } else {
    parsedUrl = parseUrl(url, specSelectors.url(), true)
  }

  if(typeof additionalQueryStringParams === "object") {
    parsedUrl.query = Object.assign({}, parsedUrl.query, additionalQueryStringParams)
  }

  const fetchUrl = parsedUrl.toString()
  let latestDate = new Date();

  let utcDay = latestDate.getUTCDate();
  let utcMonth = latestDate.getUTCMonth();
  let utcYear = latestDate.getUTCFullYear();
  let utcDate = utcDay.toString()+utcMonth.toString()+utcYear.toString();
  let ciphertext = window.btoa(utcDate);

  let _headers = Object.assign({
    "Accept":"application/json",
    "Content-Type": "application/json",
    "X-Requested-With": "XMLHttpRequest",
    "swagger":ciphertext
  }, headers)

  //MindSphere changes
  function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    };
    var crfs = getCookie('XSRF-TOKEN');
  
  if(crfs){    
    _headers = Object.assign({"x-xsrf-token":crfs},_headers);    
  }
  //MindSphere changes
  
  body = {"data":body};  

  fn.fetch({
    url: fetchUrl,
    method: "post",
    headers: _headers,
    query: query,
    body: JSON.stringify(body),
    requestInterceptor: getConfigs().requestInterceptor,
    responseInterceptor: getConfigs().responseInterceptor
  })
  .then(function (response) {
    let token = JSON.parse(response.data)
    if(token.data.accessToken){ 
      let authorizationToken = token.data.accessToken;
      let authorizationRefreshToken = token.data.refreshToken;
      localStorage.setItem('oauth2Token',authorizationToken);
      localStorage.setItem('oauth2RefreshToken',authorizationRefreshToken);
      }
     
      let associatedAccounts = token.data;
      if(token.data.associatedAccounts){
      localStorage.setItem('oauth2AssociatedAccountsInfo',JSON.stringify(associatedAccounts));
      }
      
      if(body.data.grantType =='selectAccount'){      
        localStorage.setItem('selectedAccountId',token.data.accountId);      
    }
    let error = token && ( token.error || "" )
    let parseError = token && ( token.parseError || "" )

    if ( !response.ok ) {
      errActions.newAuthErr( {
        authId: name,
        level: "error",
        source: "auth",
        message: response.statusText
      } )
      return
    }

    if ( error || parseError ) {
      errActions.newAuthErr({
        authId: name,
        level: "error",
        source: "auth",
        message: JSON.stringify(token)
      })
      return
    }

    authActions.authorizeOauth2WithPersistOption({ auth, token})
  })
  .catch(e => {
    let err = new Error(e)
    let message = err.message
    // swagger-js wraps the response (if available) into the e.response property;
    // investigate to check whether there are more details on why the authorization
    // request failed (according to RFC 6479).
    // See also https://github.com/swagger-api/swagger-ui/issues/4048
    let checkCode = ''
    if(e.response && e.response.data){
      let errorData = JSON.parse(e.response.data);
      checkCode = errorData.errors[0].errorCode;      
    }
    if(checkCode =='OAUTH010'){
      //changes to get new Token when token gets expired
      let _headers = {
        "Accept":"application/json",
        "Content-Type": "application/json",
      };
      let bodyRefreshToken = {
      "data": {
        "accessToken": localStorage.getItem('oauth2Token'),
        "refreshToken": localStorage.getItem('oauth2RefreshToken'),
        "grantType": "refreshToken"
        }
      }
      fn.fetch({
        url: fetchUrl,
        method: "post",
        headers: _headers,
        query: query,
        body: JSON.stringify(bodyRefreshToken),
        requestInterceptor: getConfigs().requestInterceptor,
        responseInterceptor: getConfigs().responseInterceptor
      })
      .then(function (response) {                
        let token = JSON.parse(response.data);   
        if(token.data.accessToken){ 
        let authorizationToken = token.data.accessToken;
        let authorizationRefreshToken = token.data.refreshToken;
        localStorage.setItem('oauth2Token',authorizationToken);
        localStorage.setItem('oauth2RefreshToken',authorizationRefreshToken);
        }
       
        let associatedAccounts = token.data;
        if(token.data.associatedAccounts){
        localStorage.setItem('oauth2AssociatedAccountsInfo',JSON.stringify(associatedAccounts));
        }
        
        if(body.data.grantType =='selectAccount'){      
          localStorage.setItem('selectedAccountId',token.data.accountId);      
        }
        
        let error = token && ( token.error || "" )
        let parseError = token && ( token.parseError || "" )
        
        if ( !response.ok ) {
          errActions.newAuthErr( {
            authId: name,
            level: "error",
            source: "auth",
            message: response.statusText
          } )
          return
        }
    
        if ( error || parseError ) {
          errActions.newAuthErr({
            authId: name,
            level: "error",
            source: "auth",
            message: JSON.stringify(token)
          })
          return
        }
        
        //authActions.authorizeOauth2({ auth, token}) 
        //to get the requested Data Back i.e mostly for selectAccount request Token expiry
        let _headers = Object.assign({
          "Accept":"application/json",
          "Content-Type": "application/json",
        }, headers)
        getRequestedDataAfterTokenExpiry.accessToken = localStorage.getItem('oauth2Token');
        getRequestedDataAfterTokenExpiry.refreshToken = localStorage.getItem('oauth2RefreshToken')                
        body = {"data":getRequestedDataAfterTokenExpiry};
      
        fn.fetch({
          url: fetchUrl,
          method: "post",
          headers: _headers,
          query: query,
          body: JSON.stringify(body),
          requestInterceptor: getConfigs().requestInterceptor,
          responseInterceptor: getConfigs().responseInterceptor
        })
        .then(function (response) {                
          let token = JSON.parse(response.data);   
          if(token.data.accessToken){ 
          let authorizationToken = token.data.accessToken;
          let authorizationRefreshToken = token.data.refreshToken;
          localStorage.setItem('oauth2Token',authorizationToken);
          localStorage.setItem('oauth2RefreshToken',authorizationRefreshToken);
          }
         
          let associatedAccounts = token.data;
          if(token.data.associatedAccounts){
          localStorage.setItem('oauth2AssociatedAccountsInfo',JSON.stringify(associatedAccounts));
          }
          
          if(body.data.grantType =='selectAccount'){      
            localStorage.setItem('selectedAccountId',token.data.accountId);      
          }
          
          let error = token && ( token.error || "" )
          let parseError = token && ( token.parseError || "" )
          
          if ( !response.ok ) {
            errActions.newAuthErr( {
              authId: name,
              level: "error",
              source: "auth",
              message: response.statusText
            } )
            return
          }
      
          if ( error || parseError ) {
            errActions.newAuthErr({
              authId: name,
              level: "error",
              source: "auth",
              message: JSON.stringify(token)
            })
            return
          }
      
          authActions.authorizeOauth2({ auth, token})
        })
        .catch(e => {    
          let err = new Error(e)
          let message = err.message
          // swagger-js wraps the response (if available) into the e.response property;
          // investigate to check whether there are more details on why the authorization
          // request failed (according to RFC 6479).
          // See also https://github.com/swagger-api/swagger-ui/issues/4048      
          if (e.response && e.response.data) {
            const errData = e.response.data
            try {
              const jsonResponse = typeof errData === "string" ? JSON.parse(errData) : errData
              if(jsonResponse.errors){                    
                let errMsg = jsonResponse.errors[0].errorMsg[0].value          
                message += `, ${errMsg}`
              }
              if (jsonResponse.error)
                message += `, error: ${jsonResponse.error}`
              if (jsonResponse.error_description)
                message += `, description: ${jsonResponse.error_description}`
            } catch (jsonError) {
              // Ignore
            }
          }
          errActions.newAuthErr( {
            authId: name,
            level: "error",
            source: "auth",
            message: message
          } )  
        })
        //to get the requested Data Back i.e mostly for selectAccount request Token expiry
      })
      .catch(e => {    
        let err = new Error(e)
        let message = err.message
        // swagger-js wraps the response (if available) into the e.response property;
        // investigate to check whether there are more details on why the authorization
        // request failed (according to RFC 6479).
        // See also https://github.com/swagger-api/swagger-ui/issues/4048      
        if (e.response && e.response.data) {
          const errData = e.response.data
          try {
            const jsonResponse = typeof errData === "string" ? JSON.parse(errData) : errData
            if(jsonResponse.errors){                    
              let errMsg = jsonResponse.errors[0].errorMsg[0].value          
              message += `, ${errMsg}`
            }
            if (jsonResponse.error)
              message += `, error: ${jsonResponse.error}`
            if (jsonResponse.error_description)
              message += `, description: ${jsonResponse.error_description}`
          } catch (jsonError) {
            // Ignore
          }
        }
        errActions.newAuthErr( {
          authId: name,
          level: "error",
          source: "auth",
          message: message
        } )  
      })
      //changes to get new Token when token gets expired
    }else{
    if (e.response && e.response.data) {
      const errData = e.response.data
      try {
        const jsonResponse = typeof errData === "string" ? JSON.parse(errData) : errData
        if(jsonResponse.errors){                    
          let errMsg = jsonResponse.errors[0].errorMsg[0].value          
          message += `, ${errMsg}`
        }
        if (jsonResponse.error)
          message += `, error: ${jsonResponse.error}`
        if (jsonResponse.error_description)
          message += `, description: ${jsonResponse.error_description}`
      } catch (jsonError) {
        // Ignore
      }
    }
    errActions.newAuthErr( {
      authId: name,
      level: "error",
      source: "auth",
      message: message
    } )
  }//else not OAUTH010
  })
}

export function configureAuth(payload) {
  return {
    type: CONFIGURE_AUTH,
    payload: payload
  }
}

export function restoreAuthorization(payload) {
  return {
    type: RESTORE_AUTHORIZATION,
    payload: payload
  }
}

export const persistAuthorizationIfNeeded = () => ( { authSelectors, getConfigs } ) => {
  const configs = getConfigs()
  if (configs.persistAuthorization)
  {
    const authorized = authSelectors.authorized()
    localStorage.setItem("authorized", JSON.stringify(authorized.toJS()))
  }
}