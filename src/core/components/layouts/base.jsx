import React from "react"
import PropTypes from "prop-types"

export default class BaseLayout extends React.Component {

  static propTypes = {
    errSelectors: PropTypes.object.isRequired,
    errActions: PropTypes.object.isRequired,
    specSelectors: PropTypes.object.isRequired,
    oas3Selectors: PropTypes.object.isRequired,
    oas3Actions: PropTypes.object.isRequired,
    getComponent: PropTypes.func.isRequired,
    getConfigs: PropTypes.func.isRequired
  }

  onChange =( e ) => {        
    global.window.location = `${location.protocol}//${location.hostname}/api/${e.target.value}/api-docs/`    
    //this.setScheme( e.target.value )
  }

  render() {
    let {errSelectors, specSelectors, getComponent,getConfigs} = this.props

    const { urls } = getConfigs()
    const { allowedAPIVersions } = getConfigs()
    const { latestAPIVersion } = getConfigs()
    
    let SvgAssets = getComponent("SvgAssets")
    let InfoContainer = getComponent("InfoContainer", true)
    let VersionPragmaFilter = getComponent("VersionPragmaFilter")
    let Operations = getComponent("operations", true)
    let Models = getComponent("Models", true)
    let Row = getComponent("Row")
    let Col = getComponent("Col")
    let Errors = getComponent("errors", true)

    const ServersContainer = getComponent("ServersContainer", true)
    const SchemesContainer = getComponent("SchemesContainer", true)
    const AuthorizeBtnContainer = getComponent("AuthorizeBtnContainer", true)
    const FilterContainer = getComponent("FilterContainer", true)
    let isSwagger2 = specSelectors.isSwagger2()
    let isOAS3 = specSelectors.isOAS3()

    const isSpecEmpty = !specSelectors.specStr()

    const loadingStatus = specSelectors.loadingStatus()

    let loadingMessage = null
  
    if(loadingStatus === "loading") {
      loadingMessage = <div className="info">
        <div className="loading-container">
          <div className="loading"></div>
        </div>
      </div>
    }

    if(loadingStatus === "failed") {
      loadingMessage = <div className="info">
        <div className="loading-container">
          <h4 className="title">Failed to load API definition.</h4>
          <Errors />
        </div>
      </div>
    }

    if (loadingStatus === "failedConfig") {
      const lastErr = errSelectors.lastError()
      const lastErrMsg = lastErr ? lastErr.get("message") : ""
      loadingMessage = <div className="info failed-config">
        <div className="loading-container">
          <h4 className="title">Failed to load remote configuration.</h4>
          <p>{lastErrMsg}</p>
        </div>
      </div>
    }

    if(!loadingMessage && isSpecEmpty) {
      loadingMessage = <h4>No API definition provided.</h4>
    }

    if(loadingMessage) {
      return <div className="swagger-ui">
        <div className="loading-container">
          {loadingMessage}
        </div>
      </div>
    }

    const servers = specSelectors.servers()
    const schemes = specSelectors.schemes()

    const hasServers = servers && servers.size
    const hasSchemes = schemes && schemes.size
    const hasSecurityDefinitions = !!specSelectors.securityDefinitions()
    let splitRequestURL = window.location.pathname.split('/');
    let selectedVersion = latestAPIVersion;

    if (splitRequestURL.length > 0 && splitRequestURL.length > 2) {
      selectedVersion = splitRequestURL[2];
    }
    let adminUrl = false;
    let adminUrlRegex = new RegExp('admin(\.[ldts])?\.qliktag\.com');
    let adminUrlRegexCheck = adminUrlRegex.test(location.host);
    if(adminUrlRegexCheck){
      adminUrl = true;
    }

    return (

      <div className='swagger-ui'>
          <SvgAssets />
          <VersionPragmaFilter isSwagger2={isSwagger2} isOAS3={isOAS3} alsoShow={<Errors/>}>
            <Errors/>
            <Row className="information-container">
              <Col mobile={12}>
                <InfoContainer/>
              </Col>
            </Row>

            {hasServers || hasSchemes || hasSecurityDefinitions ? (
              <div className="scheme-container">
                <Col className="schemes wrapper" mobile={12}>
                  {hasServers ? (<ServersContainer />) : null}
                  {hasSchemes ? (<SchemesContainer />) : null}
                  {!adminUrl ?<label htmlFor="schemes">
                  <span className="schemes-title">Version</span>
        
                  <select onChange={ this.onChange } value={selectedVersion}>
                   { urls.map(
                    url  => <option value={ url.version } key={ url.name }> { url.name }</option>)}
                  </select>
                  </label>: null }
                  {hasSecurityDefinitions ? (<AuthorizeBtnContainer />) : null}
                </Col>
              </div>
            ) : null}

            <FilterContainer/>

            <Row>
              <Col mobile={12} desktop={12} >
                <Operations/>
              </Col>
            </Row>
            <Row>
              <Col mobile={12} desktop={12} >
                <Models/>
              </Col>
            </Row>
          </VersionPragmaFilter>
        </div>
      )
  }
}
