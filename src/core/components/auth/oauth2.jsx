import React from "react"
import PropTypes from "prop-types"
import oauth2Authorize from "core/oauth2-authorize"

export default class Oauth2 extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    authorized: PropTypes.object,
    getComponent: PropTypes.func.isRequired,
    schema: PropTypes.object.isRequired,
    authSelectors: PropTypes.object.isRequired,
    authActions: PropTypes.object.isRequired,
    errSelectors: PropTypes.object.isRequired,
    oas3Selectors: PropTypes.object.isRequired,
    specSelectors: PropTypes.object.isRequired,
    errActions: PropTypes.object.isRequired,
    getConfigs: PropTypes.any
  }

  constructor(props, context) {
    super(props, context)
    let { name, schema, authorized, authSelectors } = this.props
    let auth = authorized && authorized.get(name)
    let authConfigs = authSelectors.getConfigs() || {}
    let username = auth && auth.get("username") || ""
    let email = auth && auth.get("email") || ""
    let secretKey = auth && auth.get("secretKey") || ""
    let accountUserId = auth && auth.get("accountUserId") || ""
    let applicationId = auth && auth.get("applicationId") || ""
    let applicationuserId = auth && auth.get("applicationuserId") || ""
    let clientId = auth && auth.get("clientId") || authConfigs.clientId || ""
    let clientSecret = auth && auth.get("clientSecret") || authConfigs.clientSecret || ""
    let passwordType = auth && auth.get("passwordType") || "basic"
    let grantType = auth && auth.get("grantType")
    let scopes = auth && auth.get("scopes") || authConfigs.scopes || []
    if (typeof scopes === "string") {
      scopes = scopes.split(authConfigs.scopeSeparator || " ")
    }

    this.state = {
      appName: authConfigs.appName,
      name: name,
      schema: schema,
      scopes: scopes,
      clientId: clientId,
      clientSecret: clientSecret,
      username: username,
      email:email,
      accountId:"",      
      secretKey:secretKey,
      accountUserId:accountUserId,
      applicationId:applicationId,
      applicationuserId:applicationuserId,
      password: "",
      passwordType: passwordType,
      grantType: grantType,
      selectGrantType:""
    }
  }

  close = (e) => {
    e.preventDefault()
    let { authActions } = this.props

    authActions.showDefinitions(false)
  }

  authorize =() => {
    let { authActions, errActions, getConfigs, authSelectors, oas3Selectors } = this.props
    let configs = getConfigs()
    let authConfigs = authSelectors.getConfigs()

    errActions.clear({authId: name,type: "auth", source: "auth"})
    oauth2Authorize({
      auth: this.state,
      currentServer: oas3Selectors.serverEffectiveValue(oas3Selectors.selectedServer()),
      authActions,
      errActions,
      configs,
      authConfigs
    })
  }

  onScopeChange =(e) => {
    let { target } = e
    let { checked } = target
    let scope = target.dataset.value

    if ( checked && this.state.scopes.indexOf(scope) === -1 ) {
      let newScopes = this.state.scopes.concat([scope])
      this.setState({ scopes: newScopes })
    } else if ( !checked && this.state.scopes.indexOf(scope) > -1) {
      this.setState({ scopes: this.state.scopes.filter((val) => val !== scope) })
    }
  }

  onInputChange =(e) => {
    let { target : { dataset : { name }, value } } = e
    let state = {
      [name]: value
    }

    this.setState(state)
  }

  selectScopes =(e) => {
    if (e.target.dataset.all) {
      this.setState({
        scopes: Array.from((this.props.schema.get("allowedScopes") || this.props.schema.get("scopes")).keys())
      })
    } else {
      this.setState({ scopes: [] })
    }
  }

  onSelectInputChange =(e) => {    
    let clearstate = {
      email:"",
      accountId:"",
      secretKey:"",
      accountUserId:"",
      applicationId:"",
      applicationuserId:"",
      password: ""
    }

    this.setState(clearstate)

    let { target : { dataset : { name }, value } } = e
    let state = {
      [name]: value
    }    
    
    this.setState(state)
  }

  onSelectAccountInputChange =(e) => {    
    let { target : { dataset : { name }, value } } = e        
    let state = {
      [name]: value
    }    
    
    this.setState(state)
  }

  logout =(e) => {
    e.preventDefault()
    let clearstate = {
      selectGrantType:"",
      accountId:"",
      selectedAccount:"",
      selectedAccountName:""      
    }
    this.setState(clearstate)
    let { authActions, errActions, name } = this.props

    errActions.clear({authId: name, type: "auth", source: "auth"})
    localStorage.removeItem('oauth2Token');
    localStorage.removeItem('oauth2RefreshToken');
    localStorage.removeItem('oauth2AssociatedAccountsInfo');
    localStorage.removeItem('selectedAccountId');
    authActions.logoutWithPersistOption([ name ])
  }

  selectAccount =() => {
    if(localStorage.getItem('selectedAccountId') && this.state.accountId ==''){      
      this.state.accountId = localStorage.getItem('selectedAccountId');            
    }
    
    let { authActions, errActions, getConfigs, authSelectors } = this.props        
    let configs = getConfigs()
    let authConfigs = authSelectors.getConfigs()

    errActions.clear({authId: name,type: "auth", source: "auth"})
    let associationData = this.state;    
    this.state.accessToken = localStorage.getItem('oauth2Token');
    this.state.refreshToken= localStorage.getItem('oauth2RefreshToken');
    this.state.accountId = associationData.accountId;
    this.state.selectGrantType = "selectAccount";
    oauth2Authorize({auth: this.state, authActions, errActions, configs, authConfigs })
  }

  render() {
    let {
      schema, getComponent, authSelectors, errSelectors, name, specSelectors
    } = this.props
    const Input = getComponent("Input")
    const Row = getComponent("Row")
    const Col = getComponent("Col")
    const Button = getComponent("Button")
    const AuthError = getComponent("authError")
    const JumpToPath = getComponent("JumpToPath", true)
    const Markdown = getComponent("Markdown", true)
    const InitializedInput = getComponent("InitializedInput")
    let admin = false;
    let adminUrlRegex = new RegExp('admin(\.[ldts])?\.qliktag\.com');
    let adminUrlRegexCheck = adminUrlRegex.test(location.host);
    if(adminUrlRegexCheck){
      admin = true;
    }

    const { isOAS3 } = specSelectors

    // Auth type consts
    const IMPLICIT = "implicit"
    const GRANTTYPE = "grantType"
    const ACCESS_CODE = isOAS3() ? "authorizationCode" : "accessCode"
    const APPLICATION = isOAS3() ? "clientCredentials" : "application"

    let flow = schema.get("flow")
    let scopes = schema.get("allowedScopes") || schema.get("scopes")
    let authorizedAuth = authSelectors.authorized().get(name)
    let isAuthorized = !!authorizedAuth
    let errors = errSelectors.allErrors().filter( err => err.get("authId") === name)
    let isValid = !errors.filter( err => err.get("source") === "validation").size
    let description = schema.get("description")
    let associatedAccountsInfo = '';
    if(localStorage.getItem('oauth2AssociatedAccountsInfo')){
      associatedAccountsInfo = localStorage.getItem('oauth2AssociatedAccountsInfo');                
          this.state.associatedAccountsInfo = JSON.parse(associatedAccountsInfo)
    }
    if(localStorage.getItem('oauth2AssociatedAccountsInfo') && localStorage.getItem('selectedAccountId')){
      let accountInfo = JSON.parse(associatedAccountsInfo)        
      this.state.selectedAccount = localStorage.getItem('selectedAccountId');      
      let accountName = '';

      accountInfo.associatedAccounts.forEach(element =>{
        if(element.accountId == this.state.selectedAccount){
          accountName = element.accountName;
        }
      })                 
      this.state.selectedAccountName = accountName;
    }

    return (
      <div>
        <h4>{name} (OAS3, { schema.get("flow") }) <JumpToPath path={[ "securityDefinitions", name ]} /></h4>
        { !this.state.appName ? null : <h5>Application: { this.state.appName } </h5> }
        { description && <Markdown source={ schema.get("description") } /> }

        { isAuthorized && <h6>Authorized</h6> }

        { ( flow === IMPLICIT || flow === ACCESS_CODE ) && <p>Authorization URL: <code>{ schema.get("authorizationUrl") }</code></p> }
        { ( flow === GRANTTYPE || flow === ACCESS_CODE || flow === APPLICATION ) && <p>Token URL:<code> { schema.get("tokenUrl") }</code></p> }
        <p className="flow">Flow: <code>{ schema.get("flow") }</code></p><br></br>
           {!isAuthorized ? null 
          : <Row> Authorized as <br></br>
            { this.state.email ? 
             <code> 
             <label htmlFor="oauth_email">Email:</label> { this.state.email } 
             <br></br>
             <label htmlFor="oauth_password">Password:</label> ****** </code>
             : null
            }
            { this.state.accountUserId ?  
             <code> 
             <label htmlFor="oauth_accountUserId">AccountUserId:</label> { this.state.accountUserId }
             <br></br>
             <label htmlFor="oauth_secretKey">SecretKey:</label> ****** </code>
             :null
            }
            { this.state.applicationId ?  
             <code> 
             <label htmlFor="oauth_applicationId">ApplicationId:</label> { this.state.applicationId }
             <br></br>
             <label htmlFor="oauth_secretKey">SecretKey:</label> ****** </code>
             :null
            }
            { this.state.applicationuserId ?  
             <code> 
              <label htmlFor="oauth_applicationuserId">ApplicationUserId:</label> { this.state.applicationuserId }
              <br></br>
              <label htmlFor="oauth_secretKey">SecretKey:</label> ****** </code>
             :null
            }
            <br></br>
            {
              this.state.selectedAccount ?
              <code>
              <label htmlFor="selectedAccount">Selected Account:</label> { this.state.selectedAccountName }  
              <br></br>
              </code>
              :null   
            }
            </Row>
        }       
        {
          flow !== GRANTTYPE ? null
            : <Row>
              
              <Row>                
                
                {
                  isAuthorized ? <code> { this.state.grantType } </code>
                    : <Col tablet={10} desktop={10}>
                    <label htmlFor="selectGrantType">Select Grant Type:</label><br></br><br></br>
                      <select id="selectGrantType" data-name="selectGrantType" onChange={ this.onSelectInputChange }><br></br><br></br>
                      <option value="">None</option>
                        <option value="password">Password</option>
                        {!admin ?<option value="accountUserId">AccountUserId</option>:null}
                        {!admin ?<option value="applicationId">ApplicationId</option>:null}
                        {!admin ?<option value="applicationuserId">ApplicationUserId</option>:null}
                      </select>
                    </Col>
                }
              </Row><br></br>
              
              { this.state.selectGrantType !== "password" ? null
              :<Row>
                 {isAuthorized ? null
                 :<label htmlFor="oauth_email">Email:</label>
                 }
                { 
                 isAuthorized ? null
                    : <Col tablet={10} desktop={10}>
                      <input id="oauth_email" type="text" data-name="email" onChange={ this.onInputChange }/>
                    </Col>
                }
              </Row>
              }              
              { 
                this.state.selectGrantType !== "password" ? null
                :<Row>
                 {isAuthorized ? null
                 :<label htmlFor="oauth_password">Password:</label>
                 }
                {
                  isAuthorized ? null
                    : <Col tablet={10} desktop={10}>
                      <input id="oauth_password" type="password" data-name="password" onChange={ this.onInputChange }/>
                    </Col>
                }
                </Row>
              }

              {
                this.state.selectGrantType !== "accountUserId" ? null
                :<Row>
                 {isAuthorized ? null                 
                 :<label htmlFor="oauth_accountUserId">AccountUserId:</label>
                 }
                { 
                 isAuthorized ? null
                    : <Col tablet={10} desktop={10}>
                      <input id="oauth_accountUserId" type="text" data-name="accountUserId" onChange={ this.onInputChange }/>
                    </Col>
                }
                </Row>
              }
              {
                this.state.selectGrantType !== "accountUserId" ? null
                :<Row>
                  {isAuthorized ? null                
                  :<label htmlFor="oauth_secretKey">SecretKey:</label>
                  }
                { 
                 isAuthorized ? null
                    : <Col tablet={10} desktop={10}>
                      <input id="oauth_secretKey" type="text" data-name="secretKey" onChange={ this.onInputChange }/>
                    </Col>
                }
                </Row>
              }

              {
                this.state.selectGrantType !== "applicationId" ? null
                :<Row>
                  {isAuthorized ? null                  
                  :<label htmlFor="oauth_applicationId">ApplicationId:</label>
                  }
                { 
                 isAuthorized ? null
                    : <Col tablet={10} desktop={10}>
                      <input id="oauth_applicationId" type="text" data-name="applicationId" onChange={ this.onInputChange }/>
                    </Col>
                }
                </Row>
              }
              {
                this.state.selectGrantType !== "applicationId" ? null
                :<Row>
                  {isAuthorized ? null                  
                  :<label htmlFor="oauth_secretKey">SecretKey:</label>
                  }
                { 
                 isAuthorized ? null
                    : <Col tablet={10} desktop={10}>
                      <input id="oauth_secretKey" type="text" data-name="secretKey" onChange={ this.onInputChange }/>
                    </Col>
                }
                </Row>
              }

              {
                this.state.selectGrantType !== "applicationuserId" ? null
                :<Row> 
                  {isAuthorized ? null                 
                  :<label htmlFor="oauth_applicationuserId">ApplicationUserId:</label>
                  }
                { 
                 isAuthorized ? null
                    : <Col tablet={10} desktop={10}>
                      <input id="oauth_applicationuserId" type="text" data-name="applicationuserId" onChange={ this.onInputChange }/>
                    </Col>
                }
                </Row>
              }
              {
                this.state.selectGrantType !== "applicationuserId" ? null
                :<Row>
                  {isAuthorized ? null                  
                  :<label htmlFor="oauth_secretKey">SecretKey:</label>
                  }
                { 
                 isAuthorized ? null
                    : <Col tablet={10} desktop={10}>
                      <input id="oauth_secretKey" type="text" data-name="secretKey" onChange={ this.onInputChange }/>
                    </Col>
                }
                </Row>
              }
              {
              flow !== GRANTTYPE ? null
                : <Row>
                  { !associatedAccountsInfo ? null 
                  :<div>                
                    <Col tablet={10} desktop={10}>
                    <label htmlFor="selectAccount">Account:</label>&nbsp;&nbsp;              
                    <select id="accountId" data-name="accountId" onChange={ this.onSelectAccountInputChange}>
                    {this.state.selectedAccount ? null:
                    <option value="">--SelectAccount--</option>
                    }
                    {this.state.associatedAccountsInfo.associatedAccounts.map((account) => <option key={account.accountId} value={account.accountId} selected={account.accountId == this.state.selectedAccount}>{account.accountName}</option>)}
                    </select>&nbsp;&nbsp;
                    <Button className="btn modal-btn auth updateButton" onClick={ this.selectAccount }>Update</Button>
                    </Col>
                    <br></br>
                  </div>
                  
                  }
                </Row>
             }                            
            </Row>
        }
        {
          errors.valueSeq().map( (error, key) => {
            return <AuthError error={ error }
                              key={ key }/>
          } )
        }
        <div className="auth-btn-wrapper">
        { isValid &&
          ( isAuthorized ? <Button className="btn modal-btn auth authorize" onClick={ this.logout }>Logout</Button>
        : <Button className="btn modal-btn auth authorize" onClick={ this.authorize }>Authorize</Button>
          )
        }
          <Button className="btn modal-btn auth btn-done" onClick={ this.close }>Close</Button>
        </div>

      </div>
    )
  }
}
