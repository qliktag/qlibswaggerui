### QlibSwaggerUi

## What is this repository for?
QlibSwaggerUi is created to support the qiktag changes related to swagger like following 
- Authrozation pop changes which for different grantType including select account option 
- adding information in local storage for later use
- refresh token in case access token gets expired 
- oauth API specific change to show different grantType options 
- API version related UI changes to show versions in dropdown 
- .... changes related to use for qliktag

## Setting up
- Install NVM version - 0.33.8
- Install Node version using nvm - 10.16.3
- the package json entry has been update to use qlibswaggerclient to support changes as mentioned there

# Steps
- npm install(install all modules)
- npm run dev (runs server on http://localhost:3200)
- npm run build (creates a build in dist/ folder )

the dist folder contenets will be used in qlibswaggeruidist library which gets acts as dependeny for qlibswaggeruiexpress library.